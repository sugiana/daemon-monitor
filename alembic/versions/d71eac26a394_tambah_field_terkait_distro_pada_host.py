"""Tambah field terkait distro pada host

Revision ID: d71eac26a394
Revises: 
Create Date: 2019-03-25 17:15:25.580092

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd71eac26a394'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('host', sa.Column('distro_nama', sa.String(64)))
    op.add_column('host', sa.Column('distro_versi', sa.Float))
    op.add_column('host', sa.Column('distro_codename', sa.String(64)))
    op.add_column(
            'host', sa.Column(
                'owner_id', sa.Integer, sa.ForeignKey('users.id')))


def downgrade():
    pass
