from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound
import colander
from deform import (
    Form,
    widget,
    ValidationFailure,
    )
from ..models import (
    DBSession,
    User
    )
from ..models.network import (
    VPN,
    Host,
    )


SESS_ADD_FAILED = 'host add failed'
SESS_EDIT_FAILED = 'host edit failed'

########                    
# List #
########    
@view_config(route_name='host', renderer='templates/host/list.pt',
             permission='view_trx')
def view_list(request):
    rows = DBSession.query(Host).order_by('nama')
    return dict(rows=rows)

#######    
# Add #
#######               
def daftar_ip():
    q = DBSession.query(VPN)
    r = []
    for row in q.order_by('ip'):
        r.append((row.id, row.ip))
    return r
 

@colander.deferred
def deferred_ip(node, kw):
    values = kw.get('daftar_ip', [])
    return widget.SelectWidget(values=values)


def daftar_user():
    q = DBSession.query(User).order_by(User.user_name)
    r = []
    for row in q:
        b = (row.id, row.user_name)
        r.append(b)
    return r

 
@colander.deferred
def deferred_user(node, kw):
    values = kw.get('daftar_user', [])
    return widget.SelectWidget(values=values)


class AddSchema(colander.Schema):
    id = colander.SchemaNode(colander.String(),
            widget=deferred_ip, title='IP')
    nama = colander.SchemaNode(colander.String())
    owner_id = colander.SchemaNode(
            colander.String(), widget=deferred_user, title='Pemilik')

       
@view_config(route_name='host-add', renderer='templates/host/add.pt',
             permission='view_trx')
def view_add(request):
    schema = AddSchema()
    schema = schema.bind(daftar_ip=daftar_ip(), daftar_user=daftar_user())
    form = Form(schema, buttons=('simpan', 'batalkan'))
    if request.POST:
        if 'simpan' in request.POST:
            controls = request.POST.items()
            try:
                c = form.validate(controls)
            except ValidationFailure:
                request.session[SESS_ADD_FAILED] = e.render()               
                return HTTPFound(location=request.route_url('host-add'))
            save_request(dict(controls), request)
        return route_list(request)
    elif SESS_ADD_FAILED in request.session:
        return session_failed(request, SESS_ADD_FAILED)
    return dict(form=form.render())
    
########
# Edit #
########
class EditSchema(colander.Schema):
    id = colander.SchemaNode(colander.String(),
            widget=widget.TextInputWidget(readonly=True),
            missing=colander.drop)
    nama = colander.SchemaNode(colander.String())
    owner_id = colander.SchemaNode(
            colander.String(), widget=deferred_user, title='Pemilik')
    

def save(values, row=None):
    if not row:
        row = Host()
        row.id = values['id']
    del values['id']
    row.from_dict(values)
    DBSession.add(row)
    DBSession.flush()
    return row
    
def save_request(values, request, row=None):
    if 'id' in request.matchdict:
        values['id'] = request.matchdict['id']
    row = save(values, row)
    request.session.flash('Host ID %s %s sudah disimpan dengan nama %s.' % (
        row.id, row.vpn.ip, row.nama))
        
def route_list(request):
    return HTTPFound(location=request.route_url('host'))
    
def session_failed(request, session_name):
    r = dict(form=request.session[session_name])
    del request.session[session_name]
    return r
    
def query_id(request):
    return DBSession.query(Host).filter_by(id=request.matchdict['id'])
    
def id_not_found(request):    
    msg = 'Host ID %s tidak ada.' % request.matchdict['id']
    request.session.flash(msg, 'error')
    return route_list(request)

@view_config(route_name='host-edit', renderer='templates/host/edit.pt',
             permission='view_trx')
def view_edit(request):
    row = query_id(request).first()
    if not row:
        return id_not_found(request)
    schema = EditSchema()
    schema = schema.bind(daftar_user=daftar_user())
    form = Form(schema, buttons=('simpan', 'batalkan'))        
    if request.POST:
        if 'simpan' in request.POST:
            controls = request.POST.items()
            try:
                c = form.validate(controls)
            except ValidationFailure:
                request.session[SESS_EDIT_FAILED] = e.render()               
                return HTTPFound(location=request.route_url('host-edit',
                                  id=row.id))
            save_request(dict(controls), request, row)
        return route_list(request)
    elif SESS_EDIT_FAILED in request.session:
        return session_failed(request, SESS_EDIT_FAILED)
    values = row.to_dict()
    values['ip'] = row.vpn.ip
    return dict(form=form.render(appstruct=values))

##########
# Delete #
##########    
@view_config(route_name='host-delete', renderer='templates/host/delete.pt',
             permission='view_trx')
def view_delete(request):
    q = query_id(request)
    row = q.first()
    if not row:
        return id_not_found(request)
    schema = colander.Schema()
    form = Form(schema, buttons=('hapus', 'batalkan'))
    if request.POST:
        if 'hapus' in request.POST:
            msg = 'Host ID %d %s sudah dihapus.' % (row.id, row.vpn.ip)
            q.delete()
            DBSession.flush()
            request.session.flash(msg)
        return route_list(request)
    return dict(row=row,
                 form=form.render())

