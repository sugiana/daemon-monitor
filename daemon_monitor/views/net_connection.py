from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound
import colander
from deform import (
    Form,
    widget,
    ValidationFailure,
    )
from ..models import (
    DBSession,
    User,
    )
from ..models.network import NetConnection
from ..tools import create_now


SESS_ADD_FAILED = 'net conn add failed'
SESS_EDIT_FAILED = 'net conn edit failed'
WAITING = 1 # Tabel net_status

########                    
# List #
########    
@view_config(route_name='net-conn', renderer='templates/net-conn/list.pt',
             permission='view_trx')
def view_list(request):
    rows = DBSession.query(NetConnection).order_by(NetConnection.description)
    return dict(rows=rows)

#######    
# Add #
#######               
@colander.deferred
def deferred_user(node, kw):
    values = kw.get('daftar_user', [])
    return widget.SelectWidget(values=values)
    
def get_users():
    q = DBSession.query(User)
    r = []
    for row in q.order_by(User.user_name):
        r.append((row.id, row.user_name))
    return r

class AddSchema(colander.Schema):
    description = colander.SchemaNode(colander.String())
    owner_id = colander.SchemaNode(colander.String(), widget=deferred_user)
    
        
@view_config(route_name='net-conn-add', renderer='templates/net-conn/add.pt',
             permission='view_trx')
def view_add(request):
    schema = AddSchema()
    schema = schema.bind(daftar_user=get_users())
    form = Form(schema, buttons=('save', 'cancel'))
    if request.POST:
        if 'save' in request.POST:
            controls = request.POST.items()
            try:
                c = form.validate(controls)
            except ValidationFailure:
                request.session[SESS_ADD_FAILED] = e.render()               
                return HTTPFound(location=request.route_url('net-conn-add'))
            save_request(dict(controls), request)
        return route_list(request)
    elif SESS_ADD_FAILED in request.session:
        return session_failed(request, SESS_ADD_FAILED)
    return dict(form=form.render())
    
########
# Edit #
########
class EditSchema(AddSchema):
    ip = colander.SchemaNode(colander.String(),
            widget=widget.HiddenWidget())
    
    
def save(values, row=None):
    if not row:
        row = NetConnection()
        row.ip = '0.0.0.0'
        row.status_id = WAITING
        row.updated = create_now()
    row.from_dict(values)
    DBSession.add(row)
    DBSession.flush()
    return row
    
def save_request(values, request, row=None):
    if 'id' in request.matchdict:
        values['id'] = request.matchdict['id']
    row = save(values, row)
    request.session.flash('Network connection {d} sudah disimpan.'.format(
        d=row.description))
        
def route_list(request):
    return HTTPFound(location=request.route_url('net-conn'))
    
def session_failed(request, session_name):
    r = dict(form=request.session[session_name])
    del request.session[session_name]
    return r
    
def query_id(request):
    return DBSession.query(NetConnection).filter_by(id=request.matchdict['id'])
    
def id_not_found(request):    
    msg = 'Network connection ID {id} tidak ada.'.format(
            id=request.matchdict['id'])
    request.session.flash(msg, 'error')
    return route_list(request)

@view_config(route_name='net-conn-edit', renderer='templates/net-conn/edit.pt',
             permission='view_trx')
def view_edit(request):
    row = query_id(request).first()
    if not row:
        return id_not_found(request)
    schema = EditSchema()
    schema = schema.bind(daftar_user=get_users())
    form = Form(schema, buttons=('save', 'cancel'))        
    if request.POST:
        if 'save' in request.POST:
            controls = request.POST.items()
            try:
                c = form.validate(controls)
            except ValidationFailure:
                request.session[SESS_EDIT_FAILED] = e.render()               
                return HTTPFound(location=request.route_url('net-conn-edit',
                                  id=row.id))
            save_request(dict(controls), request, row)
        return route_list(request)
    elif SESS_EDIT_FAILED in request.session:
        return session_failed(request, SESS_EDIT_FAILED)
    values = row.to_dict()
    return dict(form=form.render(appstruct=values))

##########
# Delete #
##########    
@view_config(route_name='net-conn-delete', renderer='templates/net-conn/delete.pt',
             permission='view_trx')
def view_delete(request):
    q = query_id(request)
    row = q.first()
    if not row:
        return id_not_found(request)
    schema = colander.Schema()
    form = Form(schema, buttons=('delete', 'cancel'))
    if request.POST:
        if 'delete' in request.POST:
            msg = 'Network connection ID {id} {d} sudah dihapus.'.format(
                    id=row.id, d=row.description)
            q.delete()
            DBSession.flush()
            request.session.flash(msg)
        return route_list(request)
    return dict(row=row,
                 form=form.render())

