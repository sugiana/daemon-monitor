from pyramid_rpc.xmlrpc import xmlrpc_method
from ziggurat_foundations.models.services.user import UserService
from ..models import (
    DBSession,
    User,
    )
from ..models.network import (
    HakAkses,
    NetConnection,
    VPN,
    Host,
    )
from ..tools import create_now


CODE_OK = 0
CODE_ERR_USER = -4
CODE_ERR_OWNER = -5
CODE_ERR_NOT_AVAILABLE = -55


#############
# Hak Akses #
#############
def is_allow_from_ip(params):
    q = DBSession.query(HakAkses).filter_by(
            ip=params['ip'],
            daemon=params['daemon'],
            identitas=params['identitas'])
    threads = []
    for r in q:
        threads.append(r.thread)
    return threads
                
                
#########################
# Remote Procedure Call #
#########################          
def login(username, password):
    q = DBSession.query(User).filter_by(user_name=username)
    user = q.first()
    return user and UserService.check_password(user, password) and user


def err_user(username):      
    return dict(code=CODE_ERR_USER,
                message='User {u} tidak diperkenankan'.format(u=username))
                

def err_owner(id):
    return dict(code=CODE_ERR_OWNER,
                message='Anda bukan pemilik inquiry ID {id}'.format(id=id))
    

def err_not_available(description):
    return dict(code=CODE_ERR_NOT_AVAILABLE,
                 message='Description "{d}" tidak ada'.format(d=description))


def err_ip_not_found(ip):
    return dict(code = CODE_ERR_NOT_AVAILABLE,
            message = 'IP {} tidak ada.'.format(ip))


def err_net_owner(description):
    return dict(code=CODE_ERR_OWNER,
                message='Anda bukan pemilik "{d}"'.format(d=description))


@xmlrpc_method(endpoint='rpc')
def permission(request, params):
    params['ip'] = '0.0.0.0'
    threads = is_allow_from_ip(params)
    if not threads:
        params['ip'] = request.environ['REMOTE_ADDR']
        threads = is_allow_from_ip(params)
    resp = dict(ip=request.client_addr)
    if threads:
        resp['threads'] = threads
    return resp


@xmlrpc_method(endpoint='rpc')
def net_status(self, params):
    user = login(params['username'], params['password'])
    if not user:
        return err_user(params['username'])
    q = DBSession.query(NetConnection).filter_by(
            description=params['description'])
    row = q.first()
    if not row:
        return err_not_available(params['description'])
    if row.owner_id != user.id:
        return err_net_owner(params['description'])
    is_change = False
    if row.ip != params['ip']:
        row.ip = params['ip']
        is_change = True
    port = params.get('port', None)
    if row.port != port:
        row.port = port
        is_change = True
    if row.status_id != params['status_id']:
        row.status_id = params['status_id']
        is_change = True
    if is_change:
        row.updated = create_now()
        DBSession.add(row)
        DBSession.flush()
    return dict(code=CODE_OK, message='OK')


@xmlrpc_method(endpoint='rpc')
def distro(self, params):
    user = login(params['username'], params['password'])
    if not user:
        return err_user(params['username'])
    q = DBSession.query(VPN).filter_by(ip = params['ip'])
    row = q.first()
    if not row:
        return err_ip_not_found(params['ip'])
    q = DBSession.query(Host).filter_by(id = row.id)
    row = q.first()
    if row.owner_id != user.id:
        return err_net_owner(params['ip'])
    is_change = False
    if row.distro_codename != params['codename']:
        row.distro_codename = params['codename']
        is_change =True
    if row.distro_nama != params['nama']:
        row.distro_nama = params['nama']
        is_change = True
    if row.distro_versi != params['versi']:
        row.distro_versi = params['versi']
        is_change = True
    if is_change:
        row.updated = create_now()
        DBSession.add(row)
        DBSession.flush()
    return dict(code=CODE_OK, message='OK')
