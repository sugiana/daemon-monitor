from pyramid.security import authenticated_userid
from .models import (
    User,
    UserGroup,
    DBSession,
    )

# Digunakan oleh RootFactory
def group_finder(login, request):
    u = User.get_by_identity(login)
    if not u or not u.status:
        return # None means logout
    r = []        
    for group_id in UserGroup.get_by_user(u):
        acl_name = 'group:{gid}'.format(gid=group_id)
        r.append(acl_name)
    return r
        
def get_user(request):
    username = authenticated_userid(request)
    if not username:
        return
    return User.get_by_identity(username)
