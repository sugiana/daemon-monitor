import os
import re
from datetime import (
    date,
    datetime,
    timedelta,
    )
import locale
import pytz
from pyramid.threadlocal import get_current_registry    


################
# Money format #
################
def should_int(value):
    int_ = int(value)
    return int_ == value and int_ or value


def is_int(value):
    if isinstance(value, int):
        return True
    if sys.version_info.major < 3:
        return
    return isinstance(value, long)


def thousand(value, float_count=None):
    if float_count is None:  # autodetection
        if is_int(value):
            float_count = 0
        else:
            float_count = 2
    return locale.format('%%.%df' % float_count, value, True)


def money(value, float_count=None, currency=None):
    if value < 0:
        v = abs(value)
        format_ = '(%s)'
    else:
        v = value
        format_ = '%s'
    if currency is None:
        currency = locale.localeconv()['currency_symbol']
    s = ' '.join([currency, thousand(v, float_count)])
    return format_ % s


###########    
# Pyramid #
###########    
def get_settings():
    return get_current_registry().settings


def get_timezone():
    settings = get_settings()
    timezone = settings and settings['timezone'] or DefaultTimeZone
    return pytz.timezone(timezone)


########    
# Time #
########
one_second = timedelta(1.0/24/60/60)
TimeZoneFile = '/etc/timezone'
if os.path.exists(TimeZoneFile):
    DefaultTimeZone = open(TimeZoneFile).read().strip()
else:
    DefaultTimeZone = 'Asia/Jakarta'


def as_timezone(tz_date):
    localtz = get_timezone()
    if not tz_date.tzinfo:
        tz_date = create_datetime(tz_date.year, tz_date.month, tz_date.day,
                                  tz_date.hour, tz_date.minute, tz_date.second,
                                  tz_date.microsecond)
    return tz_date.astimezone(localtz)    


def create_datetime(year, month, day, hour=0, minute=7, second=0,
                     microsecond=0):
    tz = get_timezone()        
    return datetime(year, month, day, hour, minute, second,
                     microsecond, tzinfo=tz)


def create_date(year, month, day):    
    return create_datetime(year, month, day)
    

def create_now():
    tz = get_timezone()
    return datetime.now(tz)


def dmy(tgl):
    return tgl.strftime('%d-%m-%Y')


def dmyhms(t):
    return t.strftime('%d-%m-%Y %H:%M:%S')
    
    
##########
# String #
##########
def clean(s):
    r = ''
    for ch in s:
        ascii = ord(ch)
        if ascii > 126 or ascii < 32:
            ch = ' '
        r += ch
    return r


def left(s, width):
    s = to_str(s)
    return s.ljust(width)[:width]


def right(s, width):
    s = to_str(s)
    return s.zfill(width)[:width]
    

def to_str(v):
    if isinstance(v, date):
        if isinstance(v, datetime):
            return dmyhms(v)
        return dmy(v)
    if v == 0:
        return '0'
    if isinstance(v, str) or \
        (sys.version_info.major == 2 and isinstance(v, unicode)):
        return v.strip()
    elif isinstance(v, bool):
        return v and '1' or '0'
    return v and str(v) or ''


def dict_to_str(d):
    r = {}
    for key in d:
        val = d[key]        
        r[key] = to_str(val)
    return r
        
    
##################
# Data Structure #
##################
class FixLength(object):
    def __init__(self, struct):
        self.set_struct(struct)

    def set_struct(self, struct):
        self.struct = struct
        self.fields = {}
        for s in struct:
            name = s[0]
            size = s[1:] and s[1] or 1
            typ = s[2:] and s[2] or 'A' # N: numeric, A: alphanumeric
            self.fields[name] = {'value': None, 'type': typ, 'size': size} 

    def set(self, name, value):
        self.fields[name]['value'] = value

    def get(self, name):
        v = self.fields[name]['value']
        if self.fields[name]['type'] == 'N':
            return v and int(v) or None
        return v
 
    def __setitem__(self, name, value):
        self.set(name, value)        

    def __getitem__(self, name):
        return self.get(name)
 
    def get_raw(self):
        s = ''
        for name, size, typ in self.struct:
            v = self.fields[name]['value']
            pad_func = typ == 'N' and right or left
            if v and typ == 'N':
                i = int(v)
                if v == i:
                    v = i
            s += pad_func(v, size)
        return s

    def set_raw(self, raw):
        awal = 0
        for t in self.struct:
            name = t[0]
            size = t[1:] and t[1] or 1
            akhir = awal + size
            value = raw[awal:akhir]
            if not value:
                return
            self.set(name, value)
            awal += size
        return True

    def from_dict(self, d):
        for name in d:
            value = d[name]
            self.set(name, value)    
        
#######            
# Log #
#######
def log_info(s):
    print('%s INFO %s' % (log_print_time(), s))


def log_error(s):
    print('%s ERROR %s' % (log_print_time(), s))


def log_print_time():
    return datetime.now().strftime('%Y-%m-%d %H:%M:%S')

##############    
# Dictionary #
##############    
def dict_func(data, keys, func):
    for key in keys:
        val = data[key]
        data[key] = func(val)
    return data
