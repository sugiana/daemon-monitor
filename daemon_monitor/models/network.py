from datetime import (
    datetime,
    timedelta,
    )
from sqlalchemy import (
    Column,
    Integer,
    String,
    Float,
    Boolean,
    DateTime,
    ForeignKey,
    UniqueConstraint,
    )
from sqlalchemy.orm import relationship    
from . import (
    Base,
    CommonModel,
    DefaultModel,
    )
    
    
ONE_DAY = timedelta(1)    

# Tabel bawaan paket Debian bernama openvpn-check           
class VPN(CommonModel, Base):
    __tablename__ = 'openvpn'
    id = Column(Integer, primary_key=True)
    nama = Column(String(64))
    ip = Column(String(15))
    terhubung = Column(Boolean)
    perubahan = Column(DateTime)
    
    def disconnected_too_long(self):
        if self.terhubung:
            return
        return datetime.now() - self.perubahan > ONE_DAY
                    
    
# Sebelumnya bernama Wilayah
class Host(CommonModel, Base):
    __tablename__ = 'host'
    id = Column(Integer, ForeignKey('openvpn.id'), primary_key=True)  
    nama = Column(String(64), nullable=False)
    vpn = relationship('VPN', backref='host') 
    distro_nama = Column(String(64))
    distro_versi = Column(Float)
    distro_codename = Column(String(64))
    owner_id = Column(Integer, ForeignKey('users.id'))

class NetStatus(DefaultModel, Base):
    __tablename__ = 'net_status'
    description = Column(String(64), nullable=False, unique=True)
    

class DaemonAgent(DefaultModel, Base):
    __tablename__ = 'daemon_agent'
    nama = Column(String(64), nullable=False, unique=True)
    url = Column(String(128), nullable=False, unique=True)


# Konektivitas di tempat lain yang dilaporkan ke sini
class NetConnection(DefaultModel, Base):
    __tablename__ = 'net_connection'
    description = Column(String(64), nullable=False, unique=True)
    ip = Column(String(15), nullable=False)
    port = Column(Integer)
    status_id = Column(Integer, ForeignKey('net_status.id'), nullable=False)
    status = relationship('NetStatus')
    owner_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    owner = relationship('User')
    updated = Column(DateTime(timezone=True), nullable=False)
    daemon_agent_id = Column(Integer, ForeignKey(DaemonAgent.id))
    daemon_agent = relationship(DaemonAgent, backref='net_connection')


################       
# Akses Script #
################
class HakAkses(CommonModel, Base):
    __tablename__ = 'hak_akses'
    id = Column(Integer, primary_key=True)    
    ip = Column(String(15), nullable=False) # Contoh: 10.8.10.90
    daemon = Column(String(16), nullable=False) # Contoh: pbb, pdam, padl-fix
    thread = Column(String(16), nullable=False) # Contoh: bjb, bri, pos
    identitas = Column(String(50), nullable=False) # Email
    __table_args__ = (
        UniqueConstraint('ip', 'daemon', 'thread', 'identitas'),
        )