##############        
# Filesystem #
##############        
def get_fullpath(filename):
    dir_name = os.path.split(__file__)[0]
    return os.path.join(dir_name, filename)            
