import os
import sys
import subprocess
from setuptools import (
    setup,
    find_packages,
    )


here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.rst')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()

line = CHANGES.splitlines()[0]
version = line.split()[0]

custom_requires = [
    'hg+https://bitbucket.org/dholth/cryptacular@cb96fb3#egg=cryptacular',
    ]

requires=['pyramid',
          'SQLAlchemy',
          'transaction',
          'pyramid_tm',
          'pyramid_debugtoolbar',
          'zope.sqlalchemy',          
          'waitress',
          'ziggurat-foundations',
          'colander',
          'deform',
          'pyramid_chameleon',
          'psycopg2-binary',
          'alembic',
          'pyramid_beaker',
          'pytz',
          'pyramid_rpc',          
          'bcrypt',
         ]

def pip_install(package, upgrade=False):
    cmd = [pip, 'install']
    if upgrade:
        cmd += ['--upgrade']
    if sys.argv[2:]:
        option = sys.argv[2]  # Bisa untuk proxy
        cmd += [option]
    cmd += [package]
    if subprocess.call(cmd) != 0:
        sys.exit(1)


if sys.argv[1:] and sys.argv[1] == 'develop-use-pip':
    bin_ = os.path.split(sys.executable)[0]
    pip = os.path.join(bin_, 'pip')
    pip_install('pip', True)
    pip_install('setuptools', True)
    for package in custom_requires:
        cmd = [pip, 'install', package]
        subprocess.call(cmd)
    for package in requires:
        cmd = [pip, 'install', package]
        subprocess.call(cmd)
    cmd = [sys.executable, sys.argv[0], 'develop']
    subprocess.call(cmd)
    sys.exit()

setup(name='daemon_monitor',
      version=version,
      description='daemon_monitor',
      long_description=README + '\n\n' +  CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pylons",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='',
      author_email='',
      url='',
      keywords='web pyramid pylons pbb payment gateway',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      tests_require=requires,
      test_suite="daemon_monitor",
      entry_points = """\
      [paste.app_factory]
      main = daemon_monitor:main
      [console_scripts]
      initialize_daemon_monitor_db = daemon_monitor.scripts.initializedb:main
      pbb_inquiry = daemon_monitor.scripts.pbb:inquiry
      pbb_payment = daemon_monitor.scripts.pbb:payment
      """,
      )
