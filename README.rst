Daemon Monitor
==============

Ini adalah web untuk memantau aktivitas daemon host to host beserta
status network-nya. Ia berkomunikasi dengan
`Daemon Agent <https://bitbucket.org/sugiana/daemon_agent>`_ menggunakan
protokol XML-RPC.

Dari Daemon Agent ia menerima:

1. Status konektivitas para server.
2. Profil distro yang digunakan seperti nama, versi, dan julukannya. 

Ke Daemon Agent ia mengirim:

1. Perintah restart daemon tertentu (belum dibuat)
2. Permintaan log terakhir daemon tertentu (belum dibuat)

Pemasangan
----------

Terlebih dahulu pasang paket Debian yang dibutuhkan::

    $ sudo apt-get install build-essential python-dev

Cari tahu versi Postgres::

    $ dpkg -l | grep postgresql

Lalu akan muncul seperti ini::

    ii postgresql-9.5

Sehingga yang perlu dipasang::

    $ sudo apt-get install postgresql-server-dev-9.5

Buat Python Virtual Environment::

    $ python3 -m venv ../env

Pasang paket terkait::

    $ ../env/bin/python setup.py develop-use-pip

Salin contoh konfigurasi::

    $ cp development.ini test.ini

Sesuaikan konfigurasi pada baris berikut ini::

    sqlalchemy.url = postgresql://user:pass@localhost/db
    session.url = postgresql://user:pass@localhost/db
   
Buat tabelnya::

    $ ../env/bin/initialize_daemon_agent_db test.ini

Jalankan daemon-nya::

    $ ../env/bin/pserve --reload test.ini

Semoga berhasil.
